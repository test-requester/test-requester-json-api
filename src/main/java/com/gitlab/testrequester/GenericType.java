package com.gitlab.testrequester;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * An alternative for {@code Some<T>.class} which is not valid Java syntax.
 * <p>
 * Used with {@link JsonMapper} or with {@code com.gitlab.testrequester.TestResponse} like this:
 * <pre>{@code
 *     List<String> strings = mapper.fromJson(new GenericType<List<String>>() {});
 *     List<String> strings = response.fromJsonBody(new GenericType<List<String>>() {});
 * }</pre>
 *
 * See {@code com.gitlab.testrequester.TestRequester}.
 */
@SuppressWarnings({"unused", "java:S2326"}/*T is used in get actual type arguments*/)
public abstract class GenericType<T> {
    private final Type type;

    protected GenericType() {
        Type superclass = getClass().getGenericSuperclass();
        if (superclass instanceof ParameterizedType) {
            type = ((ParameterizedType) superclass).getActualTypeArguments()[0];
        } else {
            throw new IllegalArgumentException("Missing type parameter");
        }
    }

    public Type getType() {
        return type;
    }
}
