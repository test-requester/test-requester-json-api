package com.gitlab.testrequester;

import lombok.NonNull;

import java.io.IOException;

/**
 * A plugin for TestRequester to map a java type to/from json.
 */
public interface JsonMapper {

    /**
     * Serializes a java object into json bytes.
     */
    byte[] toJsonBytes(@NonNull Object object) throws IOException;

    /**
     * Deserializes a java type from json encoded as bytes.
     */
    <T> T fromJson(@NonNull byte[] json, @NonNull Class<T> type) throws IOException;

    /**
     * Deserializes a parameterized java type from json encoded as bytes.
     */
    <T> T fromJson(@NonNull byte[] json, @NonNull GenericType<T> type) throws IOException;
}
