package com.gitlab.testrequester;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class AbstractJsonMapperTest {

    private final JsonMapper mapper = buildJsonMapper();

    protected abstract JsonMapper buildJsonMapper();

    @Test
    void test_withJsonBytes_nonGeneric() throws IOException {
        // ARRANGE
        Dto dto = new Dto(10, "foo");

        // ACT
        byte[] json = mapper.toJsonBytes(dto);
        Dto parsed = mapper.fromJson(json, Dto.class);

        // ASSERT
        assertEquals(dto, parsed);
    }

    @Test
    void test_withJsonBytes_generic() throws IOException {
        // ARRANGE
        GenericDto<Double> dto = new GenericDto<>(20, "bar", 3.14);

        // ACT
        byte[] json = mapper.toJsonBytes(dto);
        GenericDto<Double> parsed = mapper.fromJson(json, new GenericType<GenericDto<Double>>() {});

        // ASSERT
        assertEquals(dto, parsed);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Dto {
        int number;
        String text;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GenericDto<E> {
        int number;
        String text;
        E element;
    }

}
